import sys
import numpy as np
import scanpy.api as sc
import pandas as pd
import matplotlib.pyplot as pl
from matplotlib import rcParams

sc.set_figure_params(scanpy=True, dpi=80, dpi_save=200, frameon=True, vector_friendly=True, color_map=None, format='pdf', transparent=False, ipython_format='png2x')

sc.settings.verbosity = 3
data_path='../data/'
f_ext = sys.argv[1]

str_data = sc.read(data_path+'counts.csv', cache=True).T
cells_use=np.asarray(open("seurat_results/cells_use" + f_ext + ".txt").read().split())
str_data=str_data[str_data.obs_names.isin(cells_use)]
str_data=str_data[cells_use]

clusters=np.asarray(open("seurat_results/clusters" + f_ext + ".txt").read().split())
str_data.obs['louvain']=np.asarray(clusters)

pca=pd.read_csv("seurat_results/pca" + f_ext + ".csv", index_col=0)
pca=pca.loc[str_data.obs_names]
str_data.obsm['X_pca']=np.asarray(pca)

tsne=pd.read_csv("seurat_results/tsne" + f_ext + ".csv", index_col=0)
tsne=tsne.loc[str_data.obs_names]
str_data.obsm['X_tsne']=np.asarray(tsne)
sc.pp.log1p(str_data)
#sc.pl.tsne(str_data, color=['louvain'])
#sc.pl.pca(str_data, color=['louvain'])

#print(str_data.obsm['X_pca'])
sc.pp.neighbors(str_data, n_neighbors=5)
sc.tl.draw_graph(str_data)
sc.tl.louvain(str_data, resolution=0.5)
sc.tl.paga(str_data, groups='louvain')
sc.pl.paga(str_data, color=['louvain'],title='edge thresh ' + str(0.09), threshold=0.09, save=f_ext)
sc.pl.tsne(str_data, color=['louvain'],edges=True, edges_width=1, save=f_ext)
#sc.set_figure_params(scanpy=True, dpi=80, dpi_save=200, frameon=True, vector_friendly=True, color_map="Reds", format='pdf', transparent=False, ipython_format='png2x')
#sc.pl.tsne(str_data, color=['Drd1a','Tac1'],edges=True, edges_width=1, save=f_ext + '_genes1')
#sc.set_figure_params(scanpy=True, dpi=80, dpi_save=200, frameon=True, vector_friendly=True, color_map="Blues", format='pdf', transparent=False, ipython_format='png2x')
#sc.pl.tsne(str_data, color=['Drd2','Penk'],edges=True, edges_width=1, save=f_ext + '_genes2')
sc.set_figure_params(scanpy=True, dpi=80, dpi_save=200, frameon=True, vector_friendly=True, color_map="Greens", format='pdf', transparent=False, ipython_format='png2x')
#sc.pl.tsne(str_data, color=['Nxph4','Tac2'],edges=True, edges_width=1, save=f_ext + '_genes3')
sc.pl.tsne(str_data, color=['Nxph4','Pcdh8'],edges=True, edges_width=1, save=f_ext + '_genes3')

