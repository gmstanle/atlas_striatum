---
title: "R Notebook"
output: html_notebook
---
```{r setup}
require(viridis)
require(Seurat)
require(RColorBrewer)
source("pc_modification_functions.R")
source("pc_filtering_functions.R")
```



```{r load_data, echo=F}
cells.apoe <- read.csv("../iterative_clustering/1_removeAldoc/cells_apoe.csv", header = F, stringsAsFactors = F)[,1]

counts=readRDS("../data/counts.rds")
striatum <- CreateSeuratObject(raw.data = counts, min.cells = 3, min.genes = 0)
striatum=AddMetaData(object = striatum, metadata = read.csv("../data/metadata.csv", row.names=1))
striatum=SubsetData(striatum, cells.use = striatum@cell.names[!striatum@cell.names %in% cells.apoe], subset.raw = T)

striatum <- NormalizeData(striatum, scale.factor = 1e6, display.progress = F)
striatum <- ScaleData(striatum, vars.to.regress = "nReads", display.progress = F, num.cores = 8, do.par = T)
striatum <- FindVariableGenes(striatum, do.plot = F)
```

Run PCA with standard pipeline using resampling to figure out # of PCs to include
```{r noMod}

striatum <- RunPCA(striatum, do.print = F)
# striatum <- JackStraw(striatum, do.par = T, num.cores = 8)
# JackStrawPlot(striatum, PCs = 1:20) # sharp dropoff in p-values after PC15. 

dims.use=1:15
striatum <- FindClusters(striatum, dims.use = dims.use, print.output = F, force.recalc = T, resolution = 1.5)
striatum <- RunTSNE(striatum, dims.use=dims.use)
TSNEPlot(striatum, no.axes=T, coord.fixed=T)
# save.seurat.calc(striatum, dims.use, file.ext = "_noMod")

# system("/home/geoffstanley/anaconda3/bin/python calc_paga.py \"_noMod\"")

p1=TSNEPlot(striatum, do.return=T, coord.fixed=T)
p2=FeaturePlot(striatum, 'Drd1a', cols.use = c(brewer.pal(9,'Reds')[2], brewer.pal(9,'Reds')[8]), do.return = T, coord.fixed=T, no.legend = F)[[1]]
p3=FeaturePlot(striatum, 'Drd2',  cols.use = c(brewer.pal(9,'Blues')[2], brewer.pal(9,'Blues')[8]), do.return = T, no.legend=F, coord.fixed=T)[[1]]
p=plot_grid(p1, p2, p3, ncol = 3)
save_plot('SF1E_tSNE_noMod.pdf', p, ncol = 3, base_height = 2.8)

```

Sum of top 60 genes. 
```{r sumOf60, fig.height=3}
dims.use=1:15
striatum <- RunPCA(striatum, do.print = F)
old.loadings <- GetDimReduction(striatum, reduction.type = "pca", slot="gene.loadings")

# Use functions defined in pc_modification_functions.R
new.pc.scores <- reCalculatePCScores(striatum, pc.loadings = old.loadings, n.genes.pc = 30, use.binary.weights = F, genescale.method = "scale" )
striatum <- SetDimReduction(striatum, reduction.type = "pca",slot = "cell.embeddings",new.data = new.pc.scores)
striatum <- FindClusters(striatum, dims.use = dims.use, print.output = F, force.recalc = T, resolution = 1.5)

striatum <- RunTSNE(striatum, dims.use=dims.use)
TSNEPlot(striatum)
save.seurat.calc(striatum, dims.use, file.ext = "_sumOfTop60")
system("/home/geoffstanley/anaconda3/bin/python calc_paga.py \"_sumOfTop60\"")
```



# PC filtering
```{r superimpose_PCs, fig.width=6, fig.height=8}
FeaturePlot(striatum, paste0("PC", 1:15), no.axes = T, cols.use = c("navy", "olivedrab1"), no.legend = T)
```

Find pheno-orthogonal PCs <br>
1. PC7 is IEGs (likely dissociation effect)
2. PC8 is neurogenic cells (Nfib, Sp8)
3. PC10 is IEGs
4. PC14 is housekeeping and IEGs
5. PC15 is very noisy signal 
```{r choose_PO_PCs, fig.width=6, fig.height=4}
PlotTopLoadingGenes(striatum, 7)
PlotTopLoadingGenes(striatum, 8)
PlotTopLoadingGenes(striatum, 10)
PlotTopLoadingGenes(striatum, 14)
PlotTopLoadingGenes(striatum, 15)

dims.remove=c(7, 8, 10, 14, 15)
```

```{r noMod_filtered}
striatum <- RunPCA(striatum, do.print = F)
dims.use=1:15
dims.use=dims.use[!dims.use %in% dims.remove]
striatum <- FindClusters(striatum, dims.use = dims.use, print.output = F, force.recalc = T, resolution = 1.5)
striatum <- RunTSNE(striatum, dims.use=dims.use)
TSNEPlot(striatum, no.axes=T, coord.fixed=T)
save.seurat.calc(striatum, dims.use, file.ext = "_noMod_filteredPCs")

system("/home/geoffstanley/anaconda3/bin/python calc_paga.py \"_noMod_filteredPCs\"")

```

```{r sumOf60_filtered, fig.height=3}
dims.use=1:15
dims.use=dims.use[!dims.use %in% dims.remove]

striatum <- RunPCA(striatum, do.print = F)
old.loadings <- GetDimReduction(striatum, reduction.type = "pca", slot="gene.loadings")

# Use functions defined in pc_modification_functions.R
new.pc.scores <- reCalculatePCScores(striatum, pc.loadings = old.loadings, n.genes.pc = 30, use.binary.weights = F, genescale.method = "scale" )

striatum <- SetDimReduction(striatum, reduction.type = "pca",slot = "cell.embeddings",new.data = new.pc.scores)
striatum <- FindClusters(striatum, dims.use = dims.use, print.output = F, force.recalc = T, resolution = 1.5)

striatum <- RunTSNE(striatum, dims.use=dims.use)
TSNEPlot(striatum)
save.seurat.calc(striatum, dims.use, file.ext = "_sumOfTop60_filteredPCs")
system("/home/geoffstanley/anaconda3/bin/python calc_paga.py \"_sumOfTop60_filteredPCs\"")
```

```{r sumOf200_filtered (Main Fig), fig.height=3}
dims.use=1:15
dims.use=dims.use[!dims.use %in% dims.remove]

striatum <- RunPCA(striatum, do.print = F)
old.loadings <- GetDimReduction(striatum, reduction.type = "pca", slot="gene.loadings")
  
# Use functions defined in pc_modification_functions.R
new.pc.scores <- reCalculatePCScores(striatum, pc.loadings = old.loadings, n.genes.pc = 100, use.binary.weights = F, genescale.method = "scale" )

striatum <- SetDimReduction(striatum, reduction.type = "pca",slot = "cell.embeddings",new.data = new.pc.scores)
striatum <- FindClusters(striatum, dims.use = dims.use, print.output = F, force.recalc = T, resolution = 1.5)

striatum <- RunTSNE(striatum, dims.use=dims.use)
TSNEPlot(striatum)
save.seurat.calc(striatum, dims.use, file.ext = "_sumOfTop200_filteredPCs")
system("/home/geoffstanley/anaconda3/bin/python calc_paga.py \"_sumOfTop200_filteredPCs\"")
```


```{r IEG expression after filtering}
tsne=as.matrix(read.csv("seurat_results/tsne_sumOfTop200_filteredPCs.csv"))
tsne=tsne[striatum@cell.names, ]
striatum=SetDimReduction(striatum, reduction.type = 'tsne',slot='cell.embeddings', new.data = tsne)
p=FeaturePlot(striatum, c('Egr2',"Fos",'Nr4a1',"Arc"), no.axes = T, cols.use = c("grey80", "darkblue"), no.legend = T, do.return = T)
plt=plot_grid(plotlist = p, ncol=2,nrow = 2)
save_plot('IEG_afterFiltering.png', plt, ncol=2, nrow=2, base_height = 2.8)
```


# Subclustering (Not Used)
```{r subcluster_sumOfTop60}
dims.use=1:15
dims.use=dims.use[!dims.use %in% dims.remove]

striatum <- RunPCA(striatum, do.print = F)
old.loadings <- GetDimReduction(striatum, reduction.type = "pca", slot="gene.loadings")

# Use functions defined in pc_modification_functions.R
new.pc.scores <- reCalculatePCScores(striatum, pc.loadings = old.loadings, n.genes.pc = 30, use.binary.weights = F, genescale.method = "scale" )

striatum <- SetDimReduction(striatum, reduction.type = "pca",slot = "cell.embeddings",new.data = new.pc.scores)
striatum <- FindClusters(striatum, dims.use = dims.use, print.output = F, force.recalc = T, resolution = 1.5)

striatum <- RunTSNE(striatum, dims.use=dims.use)
TSNEPlot(striatum)

d1 <- SubsetData(striatum, ident.use = c(0, 3, 6, 7, 8), subset.raw = T)
d1 <- ScaleData(d1, vars.to.regress = "nReads", display.progress = F, num.cores = 8, do.par = T)
d1 <- FindVariableGenes(d1, do.plot = F, y.cutoff = 0.3)

d1 <- RunPCA(d1, do.print = F)
old.loadings <- GetDimReduction(d1, reduction.type = "pca", slot="gene.loadings")

# Use functions defined in pc_modification_functions.R
new.pc.scores <- reCalculatePCScores(d1, pc.loadings = old.loadings, n.genes.pc = 30, use.binary.weights = F, genescale.method = "scale" )

d1 <- SetDimReduction(d1, reduction.type = "pca",slot = "cell.embeddings",new.data = new.pc.scores)
d1 <- FindClusters(d1, dims.use = dims.use, print.output = F, force.recalc = T, resolution = 1.5)

d1 <- RunTSNE(d1, dims.use=dims.use)
FeaturePlot(d1, paste0("PC", 1:15), no.axes = T, cols.use = c("navy", "olivedrab1"), no.legend = T)

```
```{r}
PlotTopLoadingGenes(d1, 5) # PC 8 is poss. IEG
PlotTopLoadingGenes(d1, 15) # PC 8 is poss. IEG

```

```{r}
source("pc_modification_functions.R")

dims.use=c(1:6)
# dims.use=c(1:15)
d1 <- FindClusters(d1, dims.use = dims.use, print.output = F, force.recalc = T, resolution = 2)
d1 <- RunTSNE(d1, dims.use=dims.use)
TSNEPlot(d1)
save.seurat.calc(d1, dims.use, file.ext = "_sumOfTop60_filteredPCs_subcluster")
system("/home/geoffstanley/anaconda3/bin/python calc_paga.py \"_sumOfTop60_filteredPCs_subcluster\"")
```
Test a pair of subtypes 
```{r pairwise_PAGA}
d1h.OT <- SubsetData(d1, ident.use=c(5, 2, 7), subset.raw = T)

d1h.OT <- ScaleData(d1h.OT, vars.to.regress = "nReads", display.progress = F, num.cores = 8, do.par = T)
d1h.OT <- FindVariableGenes(d1h.OT, do.plot = F, y.cutoff = 0.5)

d1h.OT <- RunPCA(d1h.OT, do.print = F)
old.loadings <- GetDimReduction(d1h.OT, reduction.type = "pca", slot="gene.loadings")

# Use functions defined in pc_modification_functions.R
new.pc.scores <- reCalculatePCScores(d1h.OT, pc.loadings = old.loadings, n.genes.pc = 60, use.binary.weights = F, genescale.method = "scale" )

d1h.OT <- SetDimReduction(d1h.OT, reduction.type = "pca",slot = "cell.embeddings",new.data = new.pc.scores)

dims.use=c(1:3)
d1h.OT <- FindClusters(d1h.OT, dims.use = dims.use, print.output = F, force.recalc = T, resolution = .5, k.param = 10)
d1h.OT <- RunTSNE(d1h.OT, dims.use=dims.use)
pairs(GetCellEmbeddings(d1h.OT, dims.use = dims.use))

save.seurat.calc(d1h.OT, dims.use, file.ext = "_d1H_OT")
system("/home/geoffstanley/anaconda3/bin/python calc_paga.py \"_d1H_OT\"")

```


```{r}
sessionInfo()
```

