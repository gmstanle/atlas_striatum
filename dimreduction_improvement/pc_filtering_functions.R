require(Seurat)
PlotTopLoadingGenes <- function(object, dim.plot=1, ngenes=4, type=c("both", "pos","neg")){
  
  loadings <- sort(GetGeneLoadings(object=object, reduction.type = "pca",dims.use = dim.plot[1])[,1])
  genes.neg <- names(loadings)[1:ngenes]
  loadings <- sort(loadings, decreasing = T)
  genes.pos <- names(loadings)[1:ngenes]  
  
  FeaturePlot(object=object, features.plot = genes.pos, cols.use = c("grey80", "darkblue"), coord.fixed = F, pt.size = 1, no.legend = F, no.axes = T)#+
    # ggtitle(paste("PC", dim.plot, "pos genes"))
  FeaturePlot(object=object, features.plot = genes.neg, cols.use = c("grey80", "darkblue"), coord.fixed = F, pt.size = 1, no.legend = F, no.axes = T)#+
    # ggtitle(paste("PC", dim.plot, "neg genes"))

  # if(type=="both") return(plot_grid(plotlist=c(p1, p2), ncol=2))
  # else if(type=="pos") return(p1)
  # else if(type=="neg") return(p2)
  
}
