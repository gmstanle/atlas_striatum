---
title: "Analysis of RNAscope staining of striatum 17072_Kremen1_Id4_Sema5b_contra"
author: "Geoff Stanley"
date: "September 9, 2017"
output: html_document
---
  


#Load prepared data
```{r}
library(dplyr)
library(cowplot)
library(viridis)
library(spdep)
library(RColorBrewer)
library(data.table)
require(gatepoints)

dt.wholeSTR <- fread('../prepare_data/data_wholeStr.csv')

dt.wholeSTR[, cellid:=paste(ImageNumber, ObjectNumber, sep = "_")]
dt.wholeSTR <- dt.wholeSTR[Kremen1+Sema5b+Id4 > 0]
df.wholeStr=as.data.frame(dt.wholeSTR)


```




## Verification of patch id = Kremen1 + Sema5b - Id4
```{r}
# ggplot(dt.wholeSTR,aes(X,Y,color=Kremen1+Sema5b-Id4)) +
#   geom_point(size=1)+
#   coord_fixed()+
#   theme_dark() +
#   scale_color_gradientn(colors=brewer.pal(11,'Spectral')[11:1],na.value='grey20')
# 
# ggplot(dt.wholeSTR[mask_patch==F],aes(X,Y,color=Id4)) +
#   geom_point(size=1)+
#   coord_fixed()+
#   theme_dark() +
#   scale_color_gradientn(colors=brewer.pal(11,'Spectral')[11:1],na.value='grey20')
# 
# ggplot(dt.wholeSTR[mask_patch==T],aes(X,Y,color=Kremen1)) +
#   geom_point(size=1)+
#   coord_fixed()+
#   theme_dark() +
#   scale_color_gradientn(colors=brewer.pal(11,'Spectral')[11:1],na.value='grey20')
# 
# ggplot(dt.wholeSTR[mask_patch==T],aes(X,Y,color=Sema5b)) +
#   geom_point(size=1)+
#   coord_fixed()+
#   theme_dark() +
#   scale_color_gradientn(colors=brewer.pal(11,'Spectral')[11:1],na.value='grey20')
# 
# 
# ggplot(dt.wholeSTR[mask_patch==T],aes(X,Y,color=Id4)) +
#   geom_point(size=1)+
#   coord_fixed()+
#   theme_dark() +
#   scale_color_gradientn(colors=brewer.pal(11,'Spectral')[11:1],na.value='grey20')
# 
# 
# melt.wholeStr <- melt(dt.wholeSTR, id.vars = c('X.um','Y.um','mask_patch'), measure.vars = c('Kremen1','Id4','Sema5b'),
#                       variable.name = 'gene', value.name = 'expression')
# 
# ggplot(melt.wholeStr, aes(X.um, expression, color = gene)) + 
#   geom_smooth() +
#   facet_grid(gene~mask_patch) +
#   theme_bw()
# 
# ggplot(melt.wholeStr, aes(Y.um, expression, color = gene)) + 
#   geom_smooth() +
#   facet_grid(gene~mask_patch) +
#   theme_bw()

```

# Isolate a restricted spatial region to analyze patches
```{r}
data.for.selector <- as.data.frame(dt.wholeSTR[,.(X, Y)])

expr2rgb=function(x, y, z){
  x=(x-min(x))/diff(range(x))
  y=(y-min(y))/diff(range(y))
  z=(z-min(z))/diff(range(z))
  
  return(rgb(x, y, z, maxColorValue = 1))
}


row.names(data.for.selector) = dt.wholeSTR[,cellid]
X11()
plot(data.for.selector$X, data.for.selector$Y, asp=1, 
     col=expr2rgb(dt.wholeSTR$Sema5b, dt.wholeSTR$Kremen1, dt.wholeSTR$Id4),
     pch=20)
selectedPoints <- fhs(data.for.selector, mark = TRUE)

# data.msn <- dt.wholeSTR[cellid %in% as.character(selectedPoints)]
# saveRDS(selectedPoints, file="patch1_selectedPoints.Rds")
# saveRDS(selectedPoints, file="dorsalSTR_selectedPoints.rds")

saveRDS(selectedPoints, file="4patch_midDorsalSTR_selectedPoints.rds")
```

# Identify patches w/in a region
```{r}

```


```{r, fig.height=4, fig.width=4}
data.zoom <- dt.wholeSTR[cellid %in% as.character(selectedPoints)]
ggplot(data.zoom,aes(X.um,Y.um,color=Kremen1+Sema5b-Id4)) +
  geom_point(size=1)+
  coord_fixed()+
  theme_dark() +
  theme(panel.grid = element_blank())+
  scale_color_gradientn(colors=brewer.pal(11,'Spectral')[11:1],na.value='grey20', guide = F)

ggplot(data.zoom,aes(X.um,Y.um,color=Kremen1+Sema5b-Id4)) +
  geom_point(size=1)+
  coord_fixed()+
  theme_dark() +
  scale_color_viridis_c(guide=F, option = "D")
```



# Zoom in on a single patch
```{r}
selectedPoints=readRDS("patch1_selectedPoints.Rds")
data.zoom=df.wholeStr %>% filter(cellid %in% selectedPoints)
data.zoom$signature <- with(data.zoom, 0.5*(scalemax(Sema5b) + scalemax(Kremen1)) - scalemax(Id4))
plist=list()
genes=c("Id4","Kremen1","Sema5b","signature")
for(i in 1:4){
  plist[[i]]=ggplot(data.zoom,aes_string("X.um","Y.um",color=genes[i])) +
    geom_point(size=1)+
    coord_fixed()+
    theme(axis.line = element_blank(), axis.text = element_blank(), axis.title = element_blank(), axis.ticks = element_blank())+
    # theme_dark() +
    scale_color_viridis_c(option = "D", guide=F)+
    ggtitle(genes[i])
}
plot_grid(plotlist = plist, ncol=2)

```

```{r}
n_x=12
n_y=15


data.zoom$X.breaks=cut(data.zoom$X.um, breaks = n_x) # these need to be factors (default behavior)
data.zoom$Y.breaks=cut(data.zoom$Y.um, breaks = n_y)

max_x_breaks=max(as.numeric(data.zoom$X.breaks))
data.zoom$grid.id = as.numeric(data.zoom$X.breaks) + max_x_breaks*(as.numeric(data.zoom$Y.breaks)-1)


grid.zoom = data.zoom %>% group_by(grid.id) %>%
  summarize(mean.Id4=mean(Id4), mean.Sema5b=mean(Sema5b), mean.Kremen1=mean(Kremen1), mean.signature=mean(signature), 
            X.break=unique(X.breaks), Y.break = unique(Y.breaks))
grid.zoom$X.um <- midpoints(grid.zoom$X.break)
grid.zoom$Y.um <- midpoints(grid.zoom$Y.break)


ggplot(grid.zoom,aes(X.um,Y.um,color=mean.signature)) +
  geom_point(size=3)+
  coord_fixed()+
  # theme_dark() +
  scale_color_viridis()


ggplot(grid.zoom,aes(X.um,Y.um,fill=mean.signature)) +
  geom_raster()+
  coord_fixed()+
  # theme_dark() +
  scale_fill_viridis()

```
# Plot cells by grid rank
```{r, fig.height=5, fig.width=5}
data.zoom.2 <-  data.zoom %>% group_by(grid.id) %>%
  summarize(mean.signature=mean(Sema5b + Kremen1 - Id4)) %>% 
  mutate(rank.signature=rank(mean.signature)) %>%
  inner_join(data.zoom, by='grid.id')

p1=ggplot(data.zoom.2, aes(rank.signature, Id4)) +
  geom_point(alpha=0.5)+
  theme(axis.text = element_blank(), axis.title.x = element_blank())+
  geom_smooth()
p2=ggplot(data.zoom.2, aes(rank.signature, Kremen1)) +
  geom_point(alpha=0.5)+
  theme(axis.text = element_blank(), axis.title.x = element_blank())+
  geom_smooth()
p3=ggplot(data.zoom.2, aes(rank.signature, Sema5b)) +
  geom_point(alpha=0.5)+
    theme(axis.text = element_blank(), axis.title.x = element_blank())+
  geom_smooth()
p4=ggplot(data.zoom.2, aes(rank.signature, scalemax(Kremen1)+scalemax(Sema5b)-scalemax(Id4))) +
  geom_point(alpha=0.5)+
  geom_smooth()+
  ylab('signature')
plot_grid(p1, p2, p3, p4, ncol=1, rel_heights = c(1,1,1, 1.3), align='v')


data.zoom.3 <- data.zoom.2 %>% filter(rank.signature > 70)
p1=ggplot(data.zoom.3, aes(rank.signature, Id4)) +
  geom_jitter(alpha=0.3, width = .2)+
  theme(axis.text = element_blank(), axis.title.x = element_blank())
  # geom_smooth()
p2=ggplot(data.zoom.3, aes(rank.signature, Kremen1)) +
  geom_jitter(alpha=0.3, width = .2)+
  theme(axis.text = element_blank(), axis.title.x = element_blank())
  # geom_smooth()
p3=ggplot(data.zoom.3, aes(rank.signature, Sema5b)) +
  geom_jitter(alpha=0.3, width = .2)+
    theme(axis.text = element_blank(), axis.title.x = element_blank())
  # geom_smooth()
p4=ggplot(data.zoom.3, aes(rank.signature, scalemax(Kremen1)+scalemax(Sema5b)-scalemax(Id4))) +
  geom_jitter(alpha=0.3, width = .2)+
  # geom_smooth()+
  ylab('signature')
plot_grid(p1, p2, p3, p4, ncol=1, rel_heights = c(1,1,1, 1.3), align='v')
```




