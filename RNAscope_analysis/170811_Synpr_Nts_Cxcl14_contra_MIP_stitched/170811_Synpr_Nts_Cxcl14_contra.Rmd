---
title: "Analysis of RNAscope staining of striatum 170811_Synpr_Nts_Cxcl14_contra"
author: "Geoff Stanley"
date: "September 9, 2017"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
rm(list=ls())
require(data.table)
require(cowplot)
require(reshape2)
require(RColorBrewer)
require(plyr)
require(tiff)
require(doMC)
registerDoMC(2)
dt.wholeSTR <- fread('data_wholeSTR_170811_Synpr_Nts_Cxcl14.csv')

```


## Expression of genes, plotted in striatum
```{r plotWholeStr}
dt.wholeSTR[, log2_fitc := log2(fitc + 1)]
dt.wholeSTR[, log2_cy3 := log2(cy3 + 1)]
dt.wholeSTR[, log2_cy5 := log2(cy5 + 1)]

dt.wholeSTR[, Synpr := scale(log2_fitc)]
dt.wholeSTR[, Nts := scale(log2_cy3)]
dt.wholeSTR[, Cxcl14 := scale(log2_cy5)]

library(viridis)
p=ggplot(dt.wholeSTR[mask_middleICj == F],aes(X.um,Y.um,color=Nts))+
  geom_point(size=.7)+
  coord_fixed()+
  scale_color_gradientn(colors=magma(8))
save_plot('figures/wholeStr_Nts.png',p, base_height = 6)
p
display.brewer.all(
)

ggplot(dt.wholeSTR[mask_middleICj == F],aes(X.um,Y.um,color=Synpr))+
  geom_point()+
  coord_fixed()+
  scale_color_gradientn(colors=brewer.pal(11,'Spectral')[11:1],na.value='grey20') 

ggplot(dt.wholeSTR[mask_middleICj == F],aes(X.um,Y.um,color=log2_cy5))+
  geom_point()+
  coord_fixed()+
  scale_color_gradientn(colors=brewer.pal(11,'Spectral')[11:1],na.value='grey20') 


p=ggplot(dt.wholeSTR[mask_middleICj == F],aes(X.um,Y.um,color=Nts - Synpr - Cxcl14))+
  geom_point(size=.6)+
  coord_fixed()+
  scale_color_gradientn(colors=brewer.pal(11,'Spectral')[11:1],na.value='grey20', name='') 
save_plot('figures/wholeStr_Nts_minSynpr_minCxcl14.png',p, base_height = 6)

ggplot(dt.wholeSTR[mask_middleICj == F],aes(X.um,Y.um,color=Nts + Synpr - Cxcl14))+
  geom_point()+
  coord_fixed()+
  scale_color_gradientn(colors=brewer.pal(11,'Spectral')[11:1],na.value='grey20') 
```


## Remove outlier/non-striatal cells
Cxcl14 has several outliers.
```{r removeOutliers}
ggplot(dt.wholeSTR, aes(log2_cy5, log2(..count.. + 1))) +
         geom_histogram()

dt.wholeSTR[, is.outlier := F]
dt.wholeSTR[log2_cy5 > 9, is.outlier := T]

ggplot(dt.wholeSTR[mask_middleICj == F][is.outlier == F],aes(X.um,Y.um,color=log2_cy5))+
  geom_point()+
  coord_fixed()+
  scale_color_gradientn(colors=brewer.pal(11,'Spectral')[11:1],na.value='grey20') 

dt.wholeSTR.noOut <- dt.wholeSTR[is.outlier==F]
dt.wholeSTR.noOut[, Cxcl14 := scale(log2_cy5)]
```

## Quantify Nts-Cxcl14 transition
Does Synpr add information to that transistion
```{r quantifyTransition_ICj_OT}

mask.transition1 <- readTIFF('../mask_transition1_170811_Synpr_Nts_Cxcl14_contra.tif')
dim(mask.transition1)
rm(dt.mask)
dt.mask <- as.data.table(melt(mask.transition1))
setnames(dt.mask, c('Y.px', 'X.px', 'mask_transition1'))
setkey(dt.mask)
dt.wholeSTR[, mask := NULL]
dt.transition1 <- merge(dt.wholeSTR, dt.mask, by = c('X.px', 'Y.px'))
dt.transition1 <- dt.transition1[is.outlier==F][mask_transition1==1]
dt.transition1[, Synpr := scale(log2_fitc)]
dt.transition1[, Nts := scale(log2_cy3)]
dt.transition1[, Cxcl14 := scale(log2_cy5)]
rm(mask.transition1)


measurevars <- c('Cxcl14', 'Synpr', 'Nts')
mlt.transition1 <- melt(dt.transition1, id.vars = c('X.um', 'Y.um', 'mask_synprICj', 'dapi'), measure.vars = measurevars, variable.name = 'probe', value.name = 'intensity')
# mlt.transition1[, intensity.scale := scale(intensity), by = 'probe']
ggplot(mlt.transition1,aes(X.um,Y.um,color=intensity)) +
  geom_point(size=1)+
  coord_fixed()+ 
  facet_wrap(~probe) +
 scale_color_gradientn(colors=brewer.pal(11,'Spectral')[11:1],na.value='grey20') +
  geom_abline(slope = -3, intercept = 6500) +
  geom_vline(xintercept = 1700) +
  geom_abline(slope = .5, intercept = -400) +
  geom_abline(slope = 1, intercept = -1150) 

mlt.transition1[, axis.1 := Y.um]
mlt.transition1[Y.um > 1400, axis.1 := Y.um - (1/3)*X.um + 1700/3]
mlt.transition1[Y.um < X.um - 1150, axis.1 := -Y.um - 2*X.um + 3600 + 1000]

ggplot(mlt.transition1,aes(X.um,Y.um,color=axis.1)) +
  geom_point(size=1)+
  coord_fixed()+ 
 scale_color_gradientn(colors=brewer.pal(11,'Spectral')[11:1],na.value='grey20') 

ggplot(mlt.transition1, aes(axis.1, intensity, color = probe)) +
  # geom_point(alpha = .2) + 
  geom_smooth() +
  facet_grid(probe ~ .)
```


```{r transition1_binning}
n_bins = 15
# bin by Y. Empirically determine bin width based on reducing noise but not covering up signal
setorder(mlt.transition1, probe, axis.1)
mlt.transition1[, axis1.bin := cut(axis.1, breaks = n_bins), by = 'probe']
mlt.transition1[, axis.mean := mean(axis.1), by = .(probe,axis1.bin)]
means = mlt.transition1[, .(mean(intensity), mean(axis.1), sd(intensity)/sqrt(length(axis.1))), by = .(probe,axis1.bin)]

p1=ggplot(means, aes(V2, V1, group = probe, color = probe, fill = probe)) + 
  geom_line() +
  geom_ribbon(aes(x=V2, ymin=V1 - 2*V3, ymax=V1 + 2*V3), alpha = .5, color = 'white')+
  xlab('Axis (um)') +
  ylab('Expression') +
  theme(legend.position = 'none')



dt.transition1[, axis.1 := Y.um]
dt.transition1[Y.um > 1400, axis.1 := Y.um - (1/3)*X.um + 1700/3]
dt.transition1[Y.um < X.um - 1150, axis.1 := -Y.um - 2*X.um + 3600 + 1000]

dt.transition1[, nts.synpr.cxcl14 := Nts - Synpr - Cxcl14]
dt.transition1[, axis1.bin := cut(axis.1, breaks = n_bins)]
dt.transition1[, axis.mean := mean(axis.1), by = .(axis1.bin)]
means = dt.transition1[, .(mean(nts.synpr.cxcl14), mean(axis.1), sd(nts.synpr.cxcl14)/sqrt(length(axis.1))), by = .(axis1.bin)]
p2=ggplot(means, aes(V2, V1)) + 
  geom_line() +
  geom_ribbon(aes(x=V2, ymin=V1 - 2*V3, ymax=V1 + 2*V3), alpha = .5, color = 'white')+
  xlab('Axis (um)') +
  ylab('Expression')

p=plot_grid(p1, p2, nrow = 2, ncol = 1, labels = c('A', 'B'))
p
save_plot('1Danalysis_transition1_means.pdf', p, base_height = 2.5, base_aspect_ratio = 1.5, nrow = 2, ncol = 1)

```

## Axis from NAcc to OT to a single ICj
DAPI is a confound for the ICj region (although not for other regions)
How best to visualize the gradient?
- spatial rolling mean (smooth)
- spatial derivative (magnitude of gradient)
- projection along a 1D axis (difficult for transition 2)
```{r quantifyTransition_2}

# mask.transition2 <- readTIFF('../mask_transition2_170811_Synpr_Nts_Cxcl14_contra.tif')
# dim(mask.transition2)
# rm(dt.mask)
# dt.mask <- as.data.table(melt(mask.transition2))
# setnames(dt.mask, c('Y.px', 'X.px', 'mask_transition2'))
# setkey(dt.mask)
# dt.wholeSTR[, mask := NULL]
# dt.transition2 <- merge(dt.wholeSTR, dt.mask, by = c('X.px', 'Y.px'))
# dt.transition2 <- dt.transition2[is.outlier==F][mask_transition2==1]
# dt.transition2[, Synpr := scale(log2_fitc)]
# dt.transition2[, Nts := scale(log2_cy3)]
# dt.transition2[, Cxcl14 := scale(log2_cy5)]
# dt.transition2[,  nts.synpr.cxcl14 := scale(Nts - Synpr - Cxcl14)]
# save(dt.transition2, file = 'dt_transition2.RData')
# rm(mask.transition2)
load('dt_transition2.RData')

rm(mask.transition2)

dt.transition3[, dapi.scale := scale(dapi)]
measurevars <- c('Cxcl14', 'Synpr', 'Nts', 'nts.synpr.cxcl14', 'dapi.scale')
mlt.transition3 <- melt(dt.transition3, id.vars = c('X.um', 'Y.um', 'mask_synprICj'), measure.vars = measurevars, variable.name = 'probe', value.name = 'intensity')
# mlt.transition3[, intensity.scale := scale(intensity), by = 'probe']
ggplot(mlt.transition3,aes(X.um,Y.um,color=intensity)) +
  geom_point(size=1)+
  coord_fixed()+ 
  facet_wrap(~probe) +
  scale_color_gradientn(colors=brewer.pal(11,'Spectral')[11:1],na.value='grey20') +
  geom_abline(slope = -1.5, intercept = 3000) 

mlt.transition3[, axis.2 := Y.um - (1/2)*X.um]

ggplot(mlt.transition3,aes(X.um,Y.um,color=axis.2)) +
  geom_point(size=1)+
  coord_fixed()+ 
 scale_color_gradientn(colors=brewer.pal(11,'Spectral')[11:1],na.value='grey20')

ggplot(mlt.transition3, aes(axis.2, intensity, color = probe)) +
  # geom_point(alpha = .2) + 
  geom_smooth() +
  facet_grid(probe ~ .)
```
## Axis from one ICj to another through the OT
Isolate this region and manually order cells along an axis that best corresponds to gene expression.

```{r quantifyTransition3}

# mask.transition3 <- readTIFF('../mask_transition3_170811_Synpr_Nts_Cxcl14_contra.tif')
# dim(mask.transition3)
# rm(dt.mask)
# dt.mask <- as.data.table(melt(mask.transition3))
# setnames(dt.mask, c('Y.px', 'X.px', 'mask_transition3'))
# setkey(dt.mask)
# dt.wholeSTR[, mask := NULL]
# dt.transition3 <- merge(dt.wholeSTR, dt.mask, by = c('X.px', 'Y.px'))
# dt.transition3 <- dt.transition3[is.outlier==F][mask_transition3==1]
# dt.transition3[, Synpr := scale(log2_fitc)]
# dt.transition3[, Nts := scale(log2_cy3)]
# dt.transition3[, Cxcl14 := scale(log2_cy5)]
# dt.transition3[,  nts.synpr.cxcl14 := scale(Nts - Synpr - Cxcl14)]
# save(dt.transition3, file = 'dt_transition3.RData')
# rm(mask.transition3)
load('dt_transition3.RData')

measurevars <- c('Cxcl14', 'Synpr', 'Nts','nts.synpr.cxcl14')
mlt.transition3 <- melt(dt.transition3, id.vars = c('X.um', 'Y.um', 'mask_synprICj', 'dapi'), measure.vars = measurevars, variable.name = 'probe', value.name = 'intensity')
# mlt.transition3[, intensity.scale := scale(intensity), by = 'probe']
ggplot(mlt.transition3,aes(X.um,Y.um,color=intensity)) +
  geom_point(size=1)+
  coord_fixed()+ 
  facet_wrap(~probe) +
 scale_color_gradientn(colors=brewer.pal(11,'Spectral')[11:1],na.value='grey20') +
  geom_abline(slope = -1, intercept = 1850) +
  geom_vline(xintercept = 1500) +
  geom_abline(slope = .5, intercept = -400) +
  geom_abline(slope = -2, intercept = 5100)+
  geom_abline(slope = -.7, intercept = 2250)

mlt.transition3[, axis.3 := Y.um + 2*X.um ]
mlt.transition3[Y.um < -X.um + 1850, axis.3 := 1.5*Y.um + 2800]
mlt.transition3[Y.um > -.7*X.um + 2250, axis.3 := Y.um - .5*X.um + 5550]

ggplot(mlt.transition3,aes(X.um,Y.um,color=axis.3)) +
  geom_point(size=1)+
  coord_fixed()+ 
 scale_color_gradientn(colors=brewer.pal(11,'Spectral')[11:1],na.value='grey20') 

ggplot(mlt.transition3, aes(axis.3, intensity, color = probe)) +
  # geom_point(alpha = .2) + 
  geom_smooth() +
  facet_grid(probe ~ .) 
```

## Binning and quantification for transition 3 (ICj-OT-ICj axis)
Results show that the three genes show a strong gradient; however, this gradient is highly correlated to DAPI (high cell density w/in ICj). 
Really need a housekeeping gene to correct for DAPI since it is not known how much of variation is due to single cell expression changes
vs. increased cell density (especially increased density in Z, information which is lost after max intensity projection)
```{r transition3_binning}
n_bins = 20
# bin by Y. Empirically determine bin width based on reducing noise but not covering up signal
setorder(mlt.transition3, probe, axis.3)
mlt.transition3[, axis3.bin := cut(axis.3, breaks = n_bins), by = 'probe']
mlt.transition3[, axis.mean := mean(axis.3), by = .(probe,axis3.bin)]
means = mlt.transition3[, .(mean(intensity), mean(axis.3), sd(intensity)/sqrt(length(axis.3))), by = .(probe,axis3.bin)]

p1=ggplot(means[!probe %like% 'nts.*'], aes(V2, V1, group = probe, color = probe, fill = probe)) + 
  geom_ribbon(aes(x=V2, ymin=V1 - 1.8*V3, ymax=V1 + 1.8*V3), alpha = .5, color = NA)+
    geom_line() +
  xlab('Axis (um)') +
  ylab('Expression') +
  theme(legend.position = 'none')


dt.transition3[, axis.3 := Y.um + 2*X.um ]
dt.transition3[Y.um < -X.um + 1850, axis.3 := 1.5*Y.um + 2800]
dt.transition3[Y.um > -.7*X.um + 2250, axis.3 := Y.um - .5*X.um + 5550]

dt.transition3[, nts.synpr.cxcl14 := Nts - Synpr - Cxcl14]
dt.transition3[, axis3.bin := cut(axis.3, breaks = n_bins)]
dt.transition3[, axis.mean := mean(axis.3), by = .(axis3.bin)]
means = dt.transition3[, .(mean(nts.synpr.cxcl14), mean(axis.3), sd(nts.synpr.cxcl14)/sqrt(length(axis.3))), by = .(axis3.bin)]
p2=ggplot(means, aes(V2, V1)) + 
  geom_line() +
  geom_ribbon(aes(x=V2, ymin=V1 - 1.8*V3, ymax=V1 + 1.8*V3), alpha = .5, color = NA)+
  xlab('Axis (um)') +
  ylab('Expression')

p=plot_grid(p1, p2, nrow = 2, ncol = 1, labels = c('A', 'B'))
p
save_plot('1Danalysis_transition3_means.pdf', p, base_height = 2.5, base_aspect_ratio = 1.5, nrow = 2, ncol = 1)


n_bins = 20
# bin by Y. Empirically determine bin width based on reducing noise but not covering up signal
setorder(mlt.transition3, probe, axis.3)
mlt.transition3[, axis3.rank := rank(axis.3), by = 'probe']
mlt.transition3[, axis3.bin := cut(axis3.rank, breaks = n_bins), by = 'probe']
mlt.transition3[, axis.mean := mean(axis3.rank), by = .(probe,axis3.bin)]
means = mlt.transition3[, .(mean(intensity), mean(axis3.rank), sd(intensity)/sqrt(length(axis.3))), by = .(probe,axis3.bin)]

p1=ggplot(means[!probe %like% 'nts.*'], aes(V2, V1, group = probe, color = probe, fill = probe)) + 
  geom_ribbon(aes(x=V2, ymin=V1 - 1.8*V3, ymax=V1 + 1.8*V3), alpha = .5, color = NA)+
    geom_line() +
  xlab('Axis (um)') +
  ylab('Expression') +
  theme(legend.position = 'none')

dt.transition3[, axis3.rank := rank(axis.3)]
dt.transition3[, axis.3 := Y.um + 2*X.um ]
dt.transition3[Y.um < -X.um + 1850, axis.3 := 1.5*Y.um + 2800]
dt.transition3[Y.um > -.7*X.um + 2250, axis.3 := Y.um - .5*X.um + 5550]

dt.transition3[, nts.synpr.cxcl14 := Nts - Synpr - Cxcl14]
dt.transition3[, axis3.bin := cut(axis3.rank, breaks = n_bins)]
dt.transition3[, axis.mean := mean(axis3.rank), by = .(axis3.bin)]
means = dt.transition3[, .(mean(nts.synpr.cxcl14), mean(axis3.rank), sd(nts.synpr.cxcl14)/sqrt(length(axis.3))), by = .(axis3.bin)]
p2=ggplot(means, aes(V2, V1)) + 
  geom_line() +
  geom_ribbon(aes(x=V2, ymin=V1 - 1.8*V3, ymax=V1 + 1.8*V3), alpha = .5, color = NA)+
  xlab('Axis (um)') +
  ylab('Expression')

p=plot_grid(p1, p2, nrow = 2, ncol = 1, labels = c('A', 'B'))
p
save_plot('1Danalysis_transition3_means_rankOrder.pdf', p, base_height = 2.5, base_aspect_ratio = 1.5, nrow = 2, ncol = 1)

means = dt.transition3[, .(mean(dapi), mean(axis3.rank), sd(dapi)/sqrt(length(axis.3))), by = .(axis3.bin)]
rm(p)
p=ggplot(means, aes(V2, V1)) + 
  geom_line() +
  geom_ribbon(aes(x=V2, ymin=V1 - 1.8*V3, ymax=V1 + 1.8*V3), alpha = .5, color = NA)+
  xlab('Axis (rank)') +
  ylab('DAPI')
save_plot('1Danalysis_transition3_dapi_rankOrder.pdf', p, base_height = 2.5, base_aspect_ratio = 1.5, nrow = 1, ncol = 1)

# separate into 3 parts
means = dt.transition3[, .(mean(nts.synpr.cxcl14), mean(axis3.rank), sd(nts.synpr.cxcl14)/sqrt(length(axis.3)), unique(mask_synprICj)), by = .(axis3.bin)]
ggplot(means, aes(V2, V1, color = factor(V4), group = factor(V4), fill = factor(V4))) + 
  geom_line() +
  geom_ribbon(aes(x=V2, ymin=V1 - 1.8*V3, ymax=V1 + 1.8*V3), alpha = .5, color = NA)+
  xlab('Axis (um)') +
  ylab('Expression')



```
