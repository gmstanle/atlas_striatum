---
title: "R Notebook"
output:
  html_document:
    df_print: paged
---

Analysis of D1H discreteness in Tac1-Tac2_Penk stain, 122016_Tac1_Tac2_Penk contra.

```{r}
rm(list=ls())

require(data.table)
require(cowplot)
require(reshape2)
require(RColorBrewer)
require(plyr)
require(gatepoints)
require(spdep)

# setwd('~/VM-shared/RNAscope/122016_Tac1_Tac2_Penk_contra/analysis/')
# setwd('~/VM-shared/RNAscope/122016_Tac1_Tac2_Penk_contra/spkl50_retile/')
data.str=fread('data_striatum_msn.csv')
sessionInfo()
```


```{r}
colnames(data.str)
data.str[,X.um := X * .227]
data.str[,Y.um := Y * .227]

data.str[,cellid:=paste0(ImageNumber, "_", ObjectNumber)]

```

# Gate cells around ventral stripe
```{r}
# 
# data.for.selector <- as.data.frame(data.str[,.(X, Y)])
# 
# expr2rgb=function(x, y, z){
#   x=(x-min(x))/diff(range(x))
#   y=(y-min(y))/diff(range(y))
#   z=(z-min(z))/diff(range(z))
# 
#   return(rgb(x, y, 0, maxColorValue = 1))
# }
# 
# 
# row.names(data.for.selector) = data.str[,cellid]
# X11()
# plot(data.for.selector$X, data.for.selector$Y, asp=1,
#      col=expr2rgb(log2(data.str$Tac1+.5), log2(data.str$Tac2+.5), log2(data.str$Penk+.5)),
#      pch=20)
# selectedPoints <- fhs(data.for.selector, mark = TRUE)
# 
# # data.str <- data.str[cellid %in% as.character(selectedPoints)]
# # saveRDS(selectedPoints, file="patch1_selectedPoints.Rds")
# # saveRDS(selectedPoints, file="dorsalSTR_selectedPoints.rds")
# 
# saveRDS(selectedPoints, file="vStripe_selectedpoints.rds")
```
# Gate cells in the D1H-Tac2 ventral stripe
```{r}
# cells.vstripe <- readRDS('vStripe_selectedpoints.rds')
# data.vs <- data.str[cellid %in% cells.vstripe]
# 
# 
# data.for.selector <- as.data.frame(data.vs[,.(X, Y)])
# 
# expr2rgb=function(x, y, z){
#   x=(x-min(x))/diff(range(x))
#   y=(y-min(y))/diff(range(y))
#   z=(z-min(z))/diff(range(z))
# 
#   return(rgb(x, y, 0, maxColorValue = 1))
# }
# 
# 
# row.names(data.for.selector) = data.vs[,cellid]
# X11()
# plot(data.for.selector$X, data.for.selector$Y, asp=1,
#      col=expr2rgb(log2(data.vs$Tac1+.5), log2(data.vs$Tac2+.5), log2(data.vs$Penk+.5)),
#      pch=20)
# selectedPoints <- fhs(data.for.selector, mark = TRUE)
# 
# # data.vs <- data.vs[cellid %in% as.character(selectedPoints)]
# # saveRDS(selectedPoints, file="patch1_selectedPoints.Rds")
# # saveRDS(selectedPoints, file="dorsalSTR_selectedPoints.rds")
# 
# saveRDS(selectedPoints, file="vStripe_D2H_selectedpoints.rds")

```

# Gate non-D1H cells dorsal of the ventral stripe
```{r}
# cells.vstripe <- readRDS('vStripe_selectedpoints.rds')
# data.vs <- data.str[cellid %in% cells.vstripe]
# 
# 
# data.for.selector <- as.data.frame(data.vs[,.(X, Y)])
# 
# expr2rgb=function(x, y, z){
#   x=(x-min(x))/diff(range(x))
#   y=(y-min(y))/diff(range(y))
#   z=(z-min(z))/diff(range(z))
# 
#   return(rgb(x, y, 0, maxColorValue = 1))
# }
# 
# 
# row.names(data.for.selector) = data.vs[,cellid]
# X11()
# plot(data.for.selector$X, data.for.selector$Y, asp=1,
#      col=expr2rgb(log2(data.vs$Tac1+.5), log2(data.vs$Tac2+.5), log2(data.vs$Penk+.5)),
#      pch=20)
# selectedPoints <- fhs(data.for.selector, mark = TRUE)
# 
# # data.vs <- data.vs[cellid %in% as.character(selectedPoints)]
# # saveRDS(selectedPoints, file="patch1_selectedPoints.Rds")
# # saveRDS(selectedPoints, file="dorsalSTR_selectedPoints.rds")
# 
# saveRDS(selectedPoints, file="vStripe_nonD2H_selectedpoints.rds")
```


# Select a part of the vs with its boundary perpendicular to the medial-lateral axis
May need to rotate the axes.
```{r}
cells.vs = readRDS('vStripe_D2H_selectedpoints.rds')
cells.non.vs = readRDS('vStripe_nonD2H_selectedpoints.rds')

data.vl <- data.str[cellid %in% c(cells.vs, cells.non.vs)]
data.vl <- data.vl[Tac1 > 0] # limit to Tac1+ cells, i.e. D1 and D1H 

data.vl[, region:='NAcc']
data.vl[cellid %in% cells.vs, region:='VS']
data.vl$region <- factor(data.vl$region, levels=c('VS','NAcc'))
# plt=ggplot(data.vl,aes(X.um,Y.um ,color=region))+geom_point()+
#   coord_fixed()
# plt
# 
# plt=ggplot(data.vl,aes(X.um,Y.um,color=log2(Tac2)))+geom_point()+
#   coord_fixed()
# plt
# 
# plt=ggplot(data.vl,aes(X.um,Y.um,color=Tac1*Tac2*Penk > 0))+geom_point()+
#   coord_fixed()
# plt

data.vl[, X.rot:=Rotation(as.matrix(data.vl[, .(X.um, Y.um)]), angle=-pi/9)[,1]]
data.vl[, Y.rot:=Rotation(as.matrix(data.vl[, .(X.um, Y.um)]), angle=-pi/9)[,2]]

plt=ggplot(data.vl,aes(X.rot,Y.rot,color=Tac1*Tac2*Penk > 0))+geom_point()+
  coord_fixed()
plt

plt=ggplot(data.vl,aes(X.rot,Y.rot,color=region))+geom_point()+
  coord_fixed()
plt

plt=ggplot(data.vl[X.rot > 2050],aes(X.rot,Y.rot,color=region))+geom_point()+
  coord_fixed()
plt


```

# Plot expression of D1H genes along spatial axis to examine discreteness of spatial region
Tac2 + Penk looks better than Tac2 alone, mirroring results with scRNAseq where multiple genes improves signal
```{r}
data.plot = data.vl[X.rot > 2050]

plt=ggplot(data.plot, aes(Y.rot, log2(Tac2 + 1), color=region))+
  geom_point(alpha=.3, size=.5)+
  scale_color_manual(values=c('red','blue'))+ 
  stat_smooth(size=1.5)+
  ylab('Tac2, log2')
plt
save_plot('vs_Tac2_log2.pdf', plt,base_height = 2.5, base_aspect_ratio = 1.5)

plt=ggplot(data.plot, aes(Y.rot, Tac2, color=region))+
  geom_point(alpha=.3, size=.5)+
  scale_color_manual(values=c('red','blue'))+ 
  stat_smooth(size=1.5)+
  ylab('Tac2')
plt
# save_plot('vs_Tac2.pdf', plt,base_height = 2.5, base_aspect_ratio = 1.5)

plt=ggplot(data.plot, aes(Y.rot, Penk, color=region))+
  geom_point(alpha=.3, size=.5)+
  scale_color_manual(values=c('red','blue'))+ 
  stat_smooth(size=1.5)+
  ylab('Penk')
plt

plt=ggplot(data.plot, aes(Y.rot, log2(Penk + 1), color=region))+
  geom_point(alpha=.3, size=.5)+
  scale_color_manual(values=c('red','blue'))+ 
  stat_smooth(size=1.5)+
  ylab('Penk, log2')
plt

# Penk and Tac2 have very similar magnitudes on log scale so no need to scale before summing
plt=ggplot(data.plot, aes(Y.rot, log2(Tac2 + 1) + log2(Penk + 1), color=region))+
  geom_point(alpha=.3, size=.5)+
  scale_color_manual(values=c('red','blue'))+ 
  stat_smooth(size=1.5)+
  ylab('Tac2 + Penk, log2')
plt
save_plot('vs_Tac2_Penk.pdf', plt,base_height = 2.5, base_aspect_ratio = 1.5)

```








