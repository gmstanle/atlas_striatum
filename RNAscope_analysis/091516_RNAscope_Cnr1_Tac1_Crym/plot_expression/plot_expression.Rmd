---
title: "Plot expression of a caudal Cnr1-Crym gradient"
output: html_notebook
---


```{r}
require(data.table)
require(cowplot)
require(reshape2)
require(RColorBrewer)

data.striatum = fread("../data_striatum.csv")
```

```{r}
ggplot(data.striatum[Tac1 > 0,],aes(X.um,-Y.um,color=Cnr1))+geom_point(size=.7)+
  coord_fixed()+ scale_color_gradientn(colors=brewer.pal(9,'Reds'),na.value='grey20')#+theme_dark()

ggplot(data.striatum[Tac1 > 0,],aes(X.um,-Y.um,color=Crym))+geom_point(size=.7)+
  coord_fixed()+ scale_color_gradientn(colors=brewer.pal(9,'Blues'),na.value='grey20')#+theme_dark()


p=ggplot(data.striatum[Tac1 > 0,],aes(X.um,-Y.um,color=Cnr1 - Crym))+geom_point(size=1)+
  coord_fixed()+ scale_color_gradientn(colors=brewer.pal(11,'Spectral')[11:1],na.value='grey20')#+theme_dark()
p
save_plot('cnr1_crym_caudal.pdf', p,base_height = 6, base_aspect_ratio = 1)

```

