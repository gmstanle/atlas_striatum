import sys
import numpy as np
import scanpy.api as sc
import pandas as pd
import matplotlib.pyplot as pl
from matplotlib import rcParams

f_ext = sys.argv[1]
colorPalette = sys.argv[2]
genes = sys.argv[3]

sc.set_figure_params(scanpy=True, dpi=80, dpi_save=200, frameon=True, vector_friendly=True, color_map=None, format='pdf', transparent=False, ipython_format='png2x')
results_dir = sys.argv[2]
sc.settings.writedir=results_dir + '/write/'

data=sc.read(f_ext)



